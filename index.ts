import { uuid } from 'uuidv4';

enum CurrencyEnum {
    USD,
    UAH,
}

interface ICard {
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;
    getTransaction(id: string): Transaction | undefined;
    getBalance(currency: CurrencyEnum): number;
}


class Transaction {
    id: string;
    amount: number;
    currency: CurrencyEnum;
    constructor(amount: number, currency: CurrencyEnum) {
        this.id = uuid();
        this.amount = amount;
        this.currency = currency;
    }
}
class Card implements ICard {
    transactions: Transaction[] = [];

    constructor() {
        this.transactions = [];
    }
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;
    addTransaction(arg1: Transaction | CurrencyEnum, arg2?: number): string {
        let transaction: Transaction;

        if (arg1 instanceof Transaction) {
            transaction = arg1;
        } else if (
            Object.values(CurrencyEnum).includes(arg1) && arg2 !== undefined
        ) {
            transaction = new Transaction(arg2, arg1);
        } else  {
            throw new Error("Error in arg!");
        }
        this.transactions.push(transaction);
        return transaction.id;
    }

    getTransaction(id: string): Transaction | undefined {
        return this.transactions.find(transaction => transaction.id === id)
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions
            .filter(transaction => transaction.currency === currency)
            .reduce((total, transaction) => total + transaction.amount, 0);
    }
}

class BonusCard extends Card implements ICard {
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;
    addTransaction(arg1: Transaction | CurrencyEnum, arg2?: number): string {
        let transaction: Transaction;

        if (arg1 instanceof Transaction) {
            transaction = arg1;
        } else if (
            Object.values(CurrencyEnum).includes(arg1) && arg2 !== undefined
        ) {
            transaction = new Transaction(arg2, arg1);
        } else {
            throw new Error("Error in arg!");
        }
        const bonusAmount = transaction.amount * 0.1;
        const bonusTransaction = new Transaction(bonusAmount, transaction.currency);
        this.transactions.push(transaction);
        this.transactions.push(bonusTransaction);

        return transaction.id
    }
}

class Pocket {
    private cards:{[name: string]: ICard } = {};

    AddCard(name: string, card: ICard): void {
        this.cards[name] = card;
    }

    RemoveCard(name:string): void {
        delete this.cards[name];
    }

    GetCard(name: string): ICard | undefined {
        return this.cards[name];
    }

    GetTotalAmount(currency: CurrencyEnum): number {
        let totalAmount = 0;
        for (const name in this.cards) {
            if(this.cards.hasOwnProperty(name)) {
                totalAmount += this.cards[name].getBalance(currency);
            }
        }
        return totalAmount;
    }
}

export { CurrencyEnum, Transaction, ICard, Card, Pocket, BonusCard };
