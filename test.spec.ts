import {Card, CurrencyEnum, Pocket} from './index';
import {describe, expect, test} from '@jest/globals';

const card = new Card();
describe('Pocket', () => {
    let pocket: Pocket;

    beforeEach(() => {
        pocket = new Pocket();
    });

    test('AddCard adds a card to the pocket', () => {

        pocket.AddCard('card1', card);
        expect(pocket.GetCard('card1')).toBe(card); // Corrected the key to 'card1'
    });

    test('RemoveCard removes a card from the pocket', () => {

    pocket.AddCard('card1', card);
    pocket.RemoveCard('card1');
    expect(pocket.GetCard('card1')).toBeUndefined();
    });

    test('GetTotalAmount returns sum of balances in the pocket a given currency', () => {
        const card1 = new Card();
        const card2 = new Card();
        card1.addTransaction(CurrencyEnum.USD, 100);
        card2.addTransaction(CurrencyEnum.USD, 110);
        pocket.AddCard('card1', card1);
        pocket.AddCard('card2', card2);
        expect(pocket.GetTotalAmount(CurrencyEnum.USD)).toBe(210);
    });

    test('GetCard returns the card added to the pocket', () => {
        pocket.AddCard('card1', card);
        expect(pocket.GetCard('card1')).toBe(card);
    });

    test('GetCard returns undefined if card with given name does not exist', () => {
        expect(pocket.GetCard('nonExistingCard')).toBeUndefined();
    })

})
